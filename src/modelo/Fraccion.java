/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Admin
 */
public class Fraccion  implements Comparable{
    
    int numerador;
    int denominador;

    public Fraccion() {
    }

    public Fraccion(int numerador, int denominador) throws Exception {
        if(denominador==0)
        {
            throw new Exception ("No se puede crear la fracción con numerador:"+numerador+" y denominador:"+denominador);
        }
        
        this.numerador = numerador;
        this.denominador = denominador;
        
        
    }

    public int getNumerador() {
        return numerador;
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public int getDenominador() {
        return denominador;
    }

    public void setDenominador(int denominador) {
        this.denominador = denominador;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.numerador;
        hash = 89 * hash + this.denominador;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        
        final  Fraccion fraccion2 = (Fraccion) obj;
        return (this.getDecimal()==fraccion2.getDecimal());
        
        
    }
    
    
    
    public float getDecimal()
    {
        return (this.numerador/this.denominador);
    }

    @Override
    public String toString() {
        return  this.numerador+"/"+this.denominador;
    }

    @Override
    public int compareTo(Object o) {
              final  Fraccion fraccion2 = (Fraccion) o;
              
              float rta=this.getDecimal()-fraccion2.getDecimal();
              // Version candida-->return (int)(rta);
              if(rta <0)
                  return -1;
              else
                  if (rta>0)
                      return 1;
                   else
                      return 0;
              
    }
    
    
    
    
    
}
